# MAVEN DEPENDENCIES EXAMPLE

This repo is an of basic Maven concepts, such as POM and dependency resolution.

The various examples shown in class lay in the relevant branches.

The basic idea is that this project consumes some util methods defined in another project of this gitlab group, 
which is defined as maven project itself, named `maven-example-dependency` and found under 
https://gitlab.com/java-ote-march-2021/maven-example-in-class/maven-example-dependency


###### EXAMPLES

* `master` branch holds the base example with a simple dependenct to `maven-example-dependency` project
* `feature-addition-and-project-version-bump` branch holds a version bump in order to get the new methods found in
  `maven-example-dependency` same named branch
* `maven-fat-jar` branch demonstrates usage of the `maven-assembly-plugin`
* `maven-plugin-development` branch uses the plugin developed in `maven-example-plugin-project` found under https://gitlab.com/java-ote-march-2021/maven-example-in-class/maven-example-plugin-project.
For more info refer to this maven project
* `use-of-jacoco-plugin-example` uses `jacoco-maven-plugin` to demonstrate simple usage of jacoco.  




