package com.learningactors.ote.maven_example;

import java.util.Arrays;

public enum InputEnum {

    ADD_NUMBERS(1),
    MULTIPLY_NUMBERS(2),
    OTHER(999999);

    InputEnum(int selection) {
        this.selection = selection;
    }

    int selection;

    public int getSelection() {
        return selection;
    }

    public static boolean containsValue(int value) {
        return Arrays.stream(InputEnum.values()).anyMatch(v -> v.getSelection() == value);
    }

    public static InputEnum getInputEnumByValue(Integer selection) {
        return Arrays.stream(InputEnum.values()).filter(v -> v.getSelection() == selection.intValue()).findFirst().orElse(OTHER);
    }
}
