package com.learningactors.ote.maven_example;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.function.BiFunction;

public class MainExample {
    public static void main(String[] args) {

        DependencyExample.helloDependency();

        Scanner scanner = new Scanner(System.in);
        InputEnum input = InputEnum.getInputEnumByValue(handleUtilInput(scanner));
        switch (input) {
            case ADD_NUMBERS:
                handleBinaryFunctionUtil(scanner, MavenDependentProjectExample::addIntegers, "Sum");
                break;
            case MULTIPLY_NUMBERS:
                handleBinaryFunctionUtil(scanner, MavenDependentProjectExample::multiplyIntegers, "Product");
                break;
            default: OTHER:
                System.out.println("Invalid selection");
        }

    }

    private static Integer handleUtilInput(Scanner scanner) {
        boolean validInput = false;
        Integer input = 0;
        while (!validInput) {
            System.out.println("Please choose util: ");
            System.out.println("=======1: Add two numbers =======");
            System.out.println("=======2: Multiply two numbers =======");
            try {
                 input = scanner.nextInt();
                 if (!isValidAppSelection(input)) {
                     throw new InputMismatchException();
                 }
                 validInput = true;
            } catch (InputMismatchException e) {
                System.out.println("Not a valid input, please try again");
                System.out.println();
                scanner.nextLine();
            }
        }

        return input;
    }


    private static void handleBinaryFunctionUtil(Scanner scanner, BiFunction<Integer, Integer, Integer> function, String functionaName) {
        System.out.println("Please provide two numbers followed by Enter");
        Integer operand1 = 0;
        Integer operand2 = 0;
        try {
            operand1 = scanner.nextInt();
            operand2 = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Not a valid number input, please try again");
            System.out.println();
        }
        int result = function.apply(operand1, operand2);
        System.out.println(String.format("%s of %d and %d is %d", functionaName, operand1, operand2, result));
    }

    private static boolean isValidAppSelection(int selection) {
        return InputEnum.containsValue(selection);
    }
}
