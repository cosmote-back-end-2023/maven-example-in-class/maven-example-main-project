package com.learningactors.ote.maven_example;

public class MavenDependentProjectExample {

    public static int addIntegers(int a, int b) {
        return DependencyExample.addInt(a, b);
    }

    public static int multiplyIntegers(int a, int b) {
        return DependencyExample.multiplyInt(a, b);
    }

}
