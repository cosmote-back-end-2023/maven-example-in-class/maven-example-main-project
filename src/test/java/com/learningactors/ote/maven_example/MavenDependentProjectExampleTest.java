package com.learningactors.ote.maven_example;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class MavenDependentProjectExampleTest {

    MavenDependentProjectExample dependencyExample = new MavenDependentProjectExample();

    @Test
    public void should_calculate_add() {
        int result = MavenDependentProjectExample.addIntegers(3, 7);

        assertEquals(10, result);
    }

    @Test
    public void should_calculate_multiply() {
        int result = MavenDependentProjectExample.multiplyIntegers(5, 255);

        assertEquals(1275, result);
    }

}